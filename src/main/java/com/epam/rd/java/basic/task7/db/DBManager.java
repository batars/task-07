package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    private final String CONNECTION_URL;

    private static final String SQL_CREATE_USER =
            "insert into users values (default, ?)";

    private static final String SQL_FIND_ALL_USERS =
            "select * from users";

    private static final String SQL_CREATE_TEAM =
            "insert into teams values (default, ?)";

    private static final String SQL_UPDATE_TEAM =
            "update teams set name=? where id=?";

    private static final String SQL_FIND_ALL_TEAMS =
            "select * from teams";

    private static final String SQL_FIND_USER_BY_LOGIN =
            "select * from users where login=?";

    private static final String SQL_FIND_TEAM_BY_NAME =
            "select * from teams where name=?";

    private static final String SQL_FIND_TEAMS_BY_USER_ID =
            "SELECT t.id, t.name FROM teams t, users_teams ut WHERE  ut.user_id = ? AND ut.team_id = t.id";

    private static final String SQL_ADD_USER_TEAMS_BY_USER_ID =
            "insert into users_teams values (?, ?)";

    private static final String SQL_DELETE_TEAM_BY_ID =
            "delete from teams where id=?";

    private static final String SQL_DELETE_USER_BY_ID =
            "delete from users where id=?";


    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        try {
            Properties prop = new Properties();
            InputStream input = new FileInputStream(System.getProperty("user.dir") + "/app.properties");
            prop.load(input);
            CONNECTION_URL = prop.getProperty("connection.url");
        } catch (IOException ex) {
            throw new IllegalStateException("File not found", ex);
        }
    }


    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL);
    }

    private void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    private Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_FIND_ALL_USERS);

            while (rs.next()) {
                users.add(extractUser(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Find all users fail", e);
        } finally {
            close(con);
        }

        return users;
    }

    public boolean insertUser(User user) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_CREATE_USER, Statement.RETURN_GENERATED_KEYS);

            int k = 1;
            pstmt.setString(k++, user.getLogin());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("User insert fail", e);
        } finally {
            close(con);
        }

        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = getConnection();
            con.setAutoCommit(false);

            pstmt = con.prepareStatement(SQL_DELETE_USER_BY_ID);

            for (User user : users) {
                pstmt.setInt(1, user.getId());
                pstmt.executeUpdate();
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con);
            throw new DBException("Users delete fail", e);
        } finally {
            close(con);
        }

        return true;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
            pstmt.setString(1, login);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                user = extractUser(rs);
            }
        } catch (SQLException e) {
            throw new DBException("User not found", e);
        } finally {
            close(con);
        }

        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_FIND_TEAM_BY_NAME);
            pstmt.setString(1, name);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                team = extractTeam(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Team not found", e);
        } finally {
            close(con);
        }

        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);

            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException e) {
            throw new DBException("Teams not found", e);
        } finally {
            close(con);
        }

        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_CREATE_TEAM, Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, team.getName());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Team insert fail", e);
        } finally {
            close(con);
        }

        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            con.setAutoCommit(false);

            pstmt = con.prepareStatement(SQL_ADD_USER_TEAMS_BY_USER_ID);

            for (Team team : teams) {
                pstmt.setInt(1, user.getId());
                pstmt.setInt(2, team.getId());

                pstmt.executeUpdate();
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con);
            throw new DBException("Add users in exist team", e);
        } finally {
            close(con);
        }

        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_FIND_TEAMS_BY_USER_ID);

            pstmt.setInt(1, user.getId());

            rs = pstmt.executeQuery();
            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Get user teams fail", e);
        } finally {
            close(con);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = getConnection();

            pstmt = con.prepareStatement(SQL_DELETE_TEAM_BY_ID);
            pstmt.setInt(1, team.getId());

            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Delete teams fail", e);
        } finally {
            close(con);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement pstmt;

        try {
            con = getConnection();
            pstmt = con.prepareStatement(SQL_UPDATE_TEAM);

            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());

            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Update team fail", e);
        } finally {
            close(con);
        }
        return true;
    }

}
